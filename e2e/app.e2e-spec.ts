import { PelliculaPage } from './app.po';

describe('pellicula App', () => {
  let page: PelliculaPage;

  beforeEach(() => {
    page = new PelliculaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
