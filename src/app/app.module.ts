import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { CatalogModule } from './catalog/catalog.module';
import { HeaderComponent, 
         FooterComponent, 
         SharedModule 
       } from './shared';

const rootRouter: ModuleWithProviders = RouterModule.forRoot([],{ useHash: true });

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    CatalogModule,
    rootRouter,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
