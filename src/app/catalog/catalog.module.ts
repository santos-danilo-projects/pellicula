import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CatalogComponent } from './catalog.component';
import { SharedModule } from '../shared';

const catalogRouting: ModuleWithProviders = RouterModule.forChild([
    {
        path: '',
        component: CatalogComponent
    }
]);

@NgModule({
    imports: [
        catalogRouting,
        SharedModule
    ],
    declarations: [
        CatalogComponent,
    ],
    providers: []
})

export class CatalogModule {}